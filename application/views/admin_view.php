<h2>Registered user</h2>

<table class="table table-hover">
	<thead>
		<tr>
			<th>
				First Name
			</th>
			<th>
				Last Name
			</th>
			<th>
				Username
			</th>
			<th>
				Age
			</th>
			<th>
				Date Registered
			</th>
		</tr>
	</thead>

	<tbody>
		<?php foreach($users as $user): ?>

		<tr>
			<?php echo "<td>". $user->first_name . "</td>"; ?>
			<?php echo "<td>". $user->last_name . "</td>"; ?>
			<?php echo "<td>". $user->username . "</td>"; ?>
			<?php echo "<td>". $user->age . "</td>"; ?>
			<?php echo "<td>". $user->date_registered . "</td>"; ?>
		<tr>

		<?php endforeach; ?>
	
	</tbody>
</table>